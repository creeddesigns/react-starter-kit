const gulp = require('gulp');
const shell = require('shelljs');
const isDev = process.env.ENVIRONMENT !== 'production';
const webpack = require('webpack');
const notifier = require('node-notifier');

function exec(command) {
  return new Promise(function(resolve, reject) {
    shell.exec(command, function (code, stdout, stderr) {
      if (code === 0) {
        return resolve();
      }
      reject();
    })
  });
}

gulp.task('clone', function(){
  return exec('git submodule update --init --recursive')
  .then(function () {
    return exec('git submodule foreach --recursive git checkout master');
  })
  .then(function () {
    return exec('git submodule foreach --recursive git pull origin master');
  });
});

gulp.task('default', ['webpack', 'views']);

gulp.task('webpack', () => {
  const webpackConfig = require('./webpack.config');
  if (isDev) {
    webpackConfig.devtool = 'sourcemap';
    webpackConfig.debug = true;
  } else {
    webpackConfig.plugins = webpackConfig.plugins.concat(
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.UglifyJsPlugin()
    );
  }
  return new Promise(function (resolve, reject) {
    webpack(webpackConfig).run(function (err, stats) {
      if (err) {
        console.log(err);
        reject(err);
      }
      if (isDev) {
        notifier.notify({
          title: 'Gulp notification',
          message: 'Scripts have been processed.',
          onLast: true
        });
      }
      resolve();
    });
  });
});

gulp.task('views', () => {
  return gulp.src('src/index.html')
  .pipe(gulp.dest('dist/'));
});

gulp.task('watch', ['default'], () => {
  gulp.watch('src/js/**/*', ['webpack'])
  gulp.watch('src/**/*.html', ['views'])
});